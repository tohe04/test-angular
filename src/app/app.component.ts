import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  isAuth = false;

  deviceOne = 'TV';
  deviceTwo = 'Computer';
  deviceThree = 'Thermostat';

  constructor() {
    setTimeout(
      () => {
        this.isAuth = true;
      }, 3000
    );
  }

  onTurnOn() {
    console.log('We turn on all');
  }
}
